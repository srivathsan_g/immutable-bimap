immutable\_bimap package
========================

.. automodule:: immutable_bimap
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   immutable_bimap.containers
   immutable_bimap.utils
