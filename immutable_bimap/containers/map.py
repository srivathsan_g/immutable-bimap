# -- coding: UTF-8 --

"""
Map
====
This module contains classes to implement Immutable map.
"""
import collections
from functools import reduce
import logging


__author__ = 'srivathsan@gyandata.com'

LOGGER=logging.getLogger(__name__)


class ImmutableMap(collections.Mapping):
    """
    Class to represent maps which are immutable, derives from Mapping in collections module.
    """

    def __init__(self,*args, **kwargs):
        self._dic = dict(*args, **kwargs)
        self._hash = None

    def __iter__(self):
        """
        Method overriding the iterator function.

        :return: the iterator of the dictionary
        :rtype: iterable
        """
        return iter(self._dic)

    def __len__(self):
        """
        Method overriding the length function.

        :return: the length of the dictionary.
        :rtype: int
        """
        return len(self._dic)

    def __str__(self):
        """
        Method overriding the string representation of the function.

        :return: the string representation of the dictionary
        :rtype: str
        """
        return str(self._dic)

    def __getitem__(self, key):
        """
        Method overriding the get property of the dictionary.

        :param key: the key which is used to obtain the value from the mapping.
        :type key: collections.Hashable

        :return: the value corresponding to the key
        :rtype: object
        """
        return self._dic[key]

    def __hash__(self):
        """
        Method to override the hashing function.

        :return: the calculated hash value of the dictionary using xor of each pair
        :rtype: int
        """
        # return hash value if it is not previously set
        if self._hash is None:

            # xor the hash values of each key-value pairs
            self._hash = reduce(lambda x, y: hash(x) ^ hash(y), self._dic.items())

        return self._hash
