# -- coding: UTF-8 --

"""
BiMap
======
This module contains classes to implement Immutable Bimaps.
"""
import collections
import logging

from immutable_bimap.containers.map import ImmutableMap
from immutable_bimap.utils.error_handle import exception_handler, ERROR_VAL_NON_HASH, \
    ERROR_MAP_TYPE, ERROR_KEY_FUNC_NOT_CALLABLE

__author__ = "srivathsan@gyandata.com"

LOGGER = logging.getLogger(__name__)


def _check_hashable(val):
    """
    Function to check if given value is hashable. This is checked to ensure bidirectional mapping.

    :param val: the value to given as entry
    :type val: object

    :return: true if hashable else false
    :rtype: bool
    """
    return isinstance(val, collections.Hashable)


class ImmutableBiMap(ImmutableMap):
    """
    Class representing an Immutable Bimap (a two way mapping).

    :ivar sort_key: the function which is used to sort the entries of the dict
    :vartype sort_key: function
    """

    def __init__(self, sort_key=None):

        # parameter check
        if sort_key and not callable(sort_key):
            raise TypeError(ERROR_KEY_FUNC_NOT_CALLABLE)

        super().__init__()
        self._dic = dict()
        self.sort_key = sort_key

    @exception_handler
    def inverse(self):
        """
        Method to provide the inverse mapping of the existing Immutable Bimap.

        :return: the inverse mapping (values as keys and keys as values).
        :rtype: :class: `immutable_bimap.containers.bimap.ImmutableBiMap`
        """

        # build an inverse map
        return ImmutableBiMap().put_all({val: key for key, val in self.items()})

    @exception_handler
    def put(self, *args, **kwargs):
        """
        Method to generate immutable bimap from non-keyword and keyword arguments.

        :param args: non-keyword arguments
        :type args:

        :param kwargs: keyword arguments
        :type kwargs:

        :return: the generated immutable bimap from the arguments
        :type: :class: `immutable_bimap.containers.bimap.ImmutableBiMap`
        """

        immutable_dict = dict()

        # if non-keyword arguments are given
        if args:

            args = list(args)

            # iterate over each key-value pair and add them
            for key, val in zip(args[::2], args[1::2]):

                # check if val is hashable to make it bidirectional
                if not _check_hashable(val):
                    raise TypeError(ERROR_VAL_NON_HASH.format(val=val))

                # if the key or val already exists in the dictionary do not add
                if key not in immutable_dict.keys() and val not in immutable_dict.values():
                    immutable_dict[key] = val

        # if key-word arguments are given
        if kwargs:

            updt_dic = dict(**kwargs)

            # iterate over key-val pair
            for key, val in updt_dic.items():

                # check if val is hashable to make it bidirectional
                if not _check_hashable(val):
                    raise TypeError(ERROR_VAL_NON_HASH.format(val=val))

                # if the key or val already exists in the dictionary do not add
                if key not in immutable_dict.keys() and val not in immutable_dict.values():
                    immutable_dict[key] = val

        # create immutable bimap and add this dict to it
        immutable_bi_map = ImmutableBiMap()

        # if sort key is given then create ordered dict
        if self.sort_key:
            immutable_bi_map._dic = collections. \
                OrderedDict({key: val for key, val in sorted(immutable_dict.items(), key=self.sort_key)})

        # else create normal dict
        else:
            immutable_bi_map._dic = immutable_dict

        LOGGER.debug(immutable_bi_map)

        return immutable_bi_map

    @exception_handler
    def put_all(self, dict_map):
        """
        Method to generate immutable bimap from a dictionary or a map

        :param dict_map: the dictionary from which the mapping must be created
        :type dict_map: dict

        :return: None
        :rtype: :class: `immutable_bimap.containers.bimap.ImmutableBiMap`
        """

        if not issubclass(type(dict_map), (collections.Mapping, dict)):
            raise TypeError(ERROR_MAP_TYPE)

        immutable_dict = dict()

        # iterate over key-val pair
        for key, val in dict_map.items():

            # if the key or val already exists in the dictionary do not add
            if key not in immutable_dict.keys() and val not in immutable_dict.values():

                # check if val is hashable to make it bidirectional
                if not _check_hashable(val):
                    raise TypeError(ERROR_VAL_NON_HASH.format(val=val))

                immutable_dict[key] = val

        # create immutable bimap and add this dict to it
        immutable_bi_map = ImmutableBiMap()

        # if sort key is given then create ordered dict
        if self.sort_key:
            immutable_bi_map._dic = collections. \
                OrderedDict({key: val for key, val in sorted(immutable_dict.items(), key=self.sort_key)})

        # else create normal dict
        else:
            immutable_bi_map._dic = immutable_dict

        LOGGER.debug(immutable_bi_map)

        return immutable_bi_map
