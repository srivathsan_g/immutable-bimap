*****************
Immutable Bimap
*****************

.. toctree::
   :caption: Usage

   usage.rst


Developer documentation
************************

.. toctree::
   :caption: Entities
   :maxdepth: 15

   immutable_bimap.containers.map
   immutable_bimap.containers.bimap

.. toctree::
   :caption: Helpers/Utilities
   :maxdepth: 15

   immutable_bimap.utils.log
   immutable_bimap.utils.error_handle


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
