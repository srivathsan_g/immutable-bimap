# -- coding: UTF-8 --

"""
Immutable BiMap Main
==========
main script to test Immutable Bimaps
"""
import logging

from immutable_bimap.containers.bimap import ImmutableBiMap
from immutable_bimap.utils.log import build_logger

__author__ = "srivathsan@gyandata.com"

LOG_CONFIG = "configs/log_config.json"

LOGGER = logging.getLogger(__name__)


def main():
    """
    main function
    """
    build_logger(LOG_CONFIG)

    map1 = ImmutableBiMap().put(1, 2)
    map2 = ImmutableBiMap(sort_key=lambda x: x[1]).put(3, 4, 5, 7, 6, 5, a=10, b=20)
    map3 = map1.put(5, 6)
    LOGGER.info(map1 is map3)
    map3 = ImmutableBiMap().put_all({7: 8, 9: (1, 2, 3)})
    for pair in map3.items():
        LOGGER.debug(str(pair))
    map_inv = map2.inverse()
    LOGGER.debug(map_inv is map2)


if __name__ == '__main__':
    main()
