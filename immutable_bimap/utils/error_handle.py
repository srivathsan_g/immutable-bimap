# -- coding: UTF-8 --

"""
Error Handling
===============
The module which contains a function to be used as decorator for exception handling
and messages for error handling
"""

import functools
import logging


__author__ = 'srivathsan@gyandata.com'

LOGGER=logging.getLogger(__name__)


def exception_handler(func):
    """
    A decorator function for func to implement entry and exit logging and exception handling.
    """

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):

        try:

            # entry and exit logging for function
            LOGGER.debug('Entering function %s', func.__name__)
            return func(*args, **kwargs)
            # LOGGER.debug('Exiting function %s', func.__name__)
            # if ret:
            # return ret

        except TypeError as ex:
            LOGGER.error('Type Error: %s', ex.args[0])

        except KeyError as ex:
            LOGGER.error('Key Error: %s', ex.args[0])

        except ValueError as ex:
            LOGGER.error('Value Error: %s', ex.args[0])

        except Exception as ex:
            LOGGER.error('Error: %s', ex.args[0])

    return func_wrapper


###########################################################################################
# Error Messages
###########################################################################################

ERROR_KEY_FUNC_NOT_CALLABLE = 'Given key function is not callable'
"""
Error message to be displayed when given key function is not callable
"""

ERROR_VAL_NON_HASH = 'Given value({val}) is not hashable'
"""
Error message to be displayed when given value is not hashable
"""

ERROR_MAP_TYPE = 'Given map type is not dict'
"""
Error message to be displayed when given map type is not dict
"""
