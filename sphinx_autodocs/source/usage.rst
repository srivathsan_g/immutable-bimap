******
Usage
******

This package aims at implementing the `ImmutableBiMap
<https://guava.dev/releases/19.0/api/docs/com/google/common/collect/ImmutableBiMap.html>`_ from Java Google Core libraries in python.
A map here is one which has entries of key, value pairs where each value is referenced by a key.
A Bimap is a map where all keys and values are unique so reversing (inverse) the key and values also provides a map.
An Immutable Bimap is where the contents of the Bimap cannot be changed.

Examples
**********

Here we will discuss some examples on how to use this package

The *ImmutableBiMap* class has a :func:`~immutable_bimap.containers.bimap.ImmutableBiMap.put` method which will be used to insert entries and return an immutable bimap.

>>> from immutable_bimap.containers.bimap import ImmutableBiMap
>>> map1 = ImmutableBiMap().put(1,2,3,4)
>>> print(map1)
{1: 2, 3: 4}

The :func:`~immutable_bimap.containers.bimap.ImmutableBiMap.put` method also accepts keyword arguments which will be treated as key-value pairs. A sort-key parameter,
passed while creating the map object, accepts a function which will be used to sort the pairs. The generated dictionary
mapping will also be an `OrderedDict
<https://docs.python.org/3.4/library/collections.html?highlight=ordereddict#collections.OrderedDict>`_.

>>> map2 = ImmutableBiMap(sort_key=lambda x: x[1]).put(3, 4, 5, 7, 6, 5, a=10, b=20)
>>> print(map2)
OrderedDict([(3, 4), (6, 5), (5, 7), ('a', 10), ('b', 20)])

When a pre-existing Immutable bimap object's put method is used it will return a new object and will not mutate the
calling one.

>>> map3 = map1.put(5, 6)
>>> print(map3)
{5: 6}
>>> map1 is map3
>>> False

To obtain an immutable bimap from any other map directly a :func:`~immutable_bimap.containers.bimap.ImmutableBiMap.put_all`
method can be used.

>>> map3 = ImmutableBiMap().put_all({7: 8, 9: (1, 2, 3)})
>>> print(map3)
{7: 8, 9: (1, 2, 3)}

Since this is a bimap, the reverse mapping relationship can be obtained by calling
:func:`~immutable_bimap.containers.bimap.ImmutableBiMap.inverse`.

>>> map_inv = map2.inverse()
>>> print(map_inv)


